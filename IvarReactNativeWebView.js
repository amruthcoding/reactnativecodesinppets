import React, { Component } from 'react';
import { Text, View, StyleSheet,WebView } from 'react-native';
import { Constants } from 'expo';

export default class App extends Component {
 render() {
    return (
      <WebView
        source={{uri: 'http://www.madhaviskitchen.com/2012/11/diwali-sweet-jangri.html?m=1'}}
        style={{marginTop: 20}}
      />
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
});
