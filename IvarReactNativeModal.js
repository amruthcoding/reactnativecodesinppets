'use strict';


import React, { Component } from 'react';
import { Text, View, StyleSheet, WebView,TouchableHighlight,Modal } from 'react-native';


export default class App extends Component {
  
  state = {
    modalVisible: false,
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
 render() {
    return (
    <View style={{marginTop: 22}}>
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalVisible}>
             <View style={styles.hideModal}>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(!this.state.modalVisible)
                }}>
                    <Text style={{fontSize:25}}>Hide Modal</Text>
                </TouchableHighlight>
            </View>  
            <View style={{flex:1}}>
                <WebView
                source={{uri: 'http://www.madhaviskitchen.com/2012/11/diwali-sweet-jangri.html?m=1'}}
                style={{marginTop: 20}}/>
              </View>
           
       
        </Modal>

        <TouchableHighlight onPress={() => {this.setModalVisible(true)}}>
          <Text style={styles.showModal}>Show Modal</Text>
        </TouchableHighlight>

      </View>
    );
  }
}



const styles = StyleSheet.create({
    showModal : {
      fontSize: 20
    },
    hideModal : {
      marginTop:20
    }
});