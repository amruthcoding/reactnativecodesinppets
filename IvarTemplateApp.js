import React, { Component } from 'react';
import { Text, 
         View, 
         StyleSheet,
         ScrollView,
         Dimensions,
         Image
} from 'react-native';



/* 
*   App template 
*       Header - optional icons 10%
*       Body - 80%
*       Footer - optional ( 10%)
*
*      User Dimensional object
*
*/



export default class IvarTemplateApp extends Component {
  
   constructor(props) {
      super(props);
   }
   
   
  render() {
    return (
      <View style={styles.container}>
         {this.renderHeader()}
         {this.renderBody()}
         {this.renderFooter()}
       
      </View>
    );
  }
  
  renderHeader() {
    return (
       <View style={this._getCSS(styles.header,15)}>
            <Text style= {styles.title}> Header </Text>
          
         </View>
         )
  }
  
  renderBody() {
    return (
    <ScrollView>
      <Text style={{fontSize:10}}> Text1 </Text>
      <Text style={{fontSize:15}}> Text2 </Text>
       <Text style={{fontSize:20}}> Text3 </Text>
        <Text style={{fontSize:25}}> Text4 </Text>
         <Text style={{fontSize:30}}> Text5 </Text>
          <Text style={{fontSize:35}}> Text6 </Text>
           <Text style={{fontSize:40}}> Text7 </Text>
            <Text style={{fontSize:45}}> Text8 </Text>
             <Text style={{fontSize:50}}> Text9 </Text>
        <Image  style={{width: 50, height: 50}} source={{uri:'https://facebook.github.io/react/img/logo_og.png'}}/>  
        <Image  style={{width: 70, height: 70}} source={{uri:'https://facebook.github.io/react/img/logo_og.png'}}/>     
        <Image  style={{width: 90, height: 90}} source={{uri:'https://facebook.github.io/react/img/logo_og.png'}}/>     
        <Image  style={{width:100, height: 100}} source={{uri:'https://facebook.github.io/react/img/logo_og.png'}}/>     
    </ScrollView>
    )
  }
  
  renderFooter() {
     return (
          <View style={this._getCSS(styles.footer,15)}>
             <Text  style= {styles.title}> FOOTER </Text>
          </View>
       )
  }
  
  _getCSS(css, height) {
    if(height)
       css = Object.assign(css,{ height : this._getHeight(height)} );
   
    
    return css;
  }
   _getWidth(percent) {
      return  Math.round(Dimensions.get('window').width * (percent/100));
    }
  _getHeight(percent) {
      return  Math.round(Dimensions.get('window').height * (percent/100));
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header : {
     marginTop: 15,
     backgroundColor: '#ccc'
     },
  footer : {
     backgroundColor: '#ccc' 
  },  
  title : {
    fontSize : 20,
     textAlign:'center'
  }   
 
});
