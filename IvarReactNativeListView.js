import React, { Component } from 'react';
import { Text, 
        View, 
        Image, 
        ListView,
        TouchableHighlight,
        StyleSheet } from 'react-native';

import { Dimensions } from 'react-native'




const RECIPES = [
    { id: 11, name: 'Recipes1', description: "Recipe1 Recipe1 Recipe1 Recipe1 Recipe1 Recipe1 Recipe1 ",
        imageUrl: "http://2.bp.blogspot.com/-XEr0HFwm8f8/UKKpvvvUPuI/AAAAAAAATC4/3CciH7zAFBc/s200/DSC_0050.JPG",
          ingre: [ "Urad dal : 1 cup"," Orange color ","2 pinches Oil for deep fry" ] },
    { id: 12, name: 'Recipes2', description: "Recipe1 Recipe1 Recipe1 Recipe1 Recipe1 Recipe1 Recipe2 ",
        imageUrl: "http://1.bp.blogspot.com/-77Incnx2KDw/UJ_wWdQMUWI/AAAAAAAAS_A/q1WGrL0dPwU/s200/DSC_0043.JPG",
          ingre: [ "Urad dal : 1 cup"," Orange color ","2 pinches Oil for deep fry" ] },
    { id: 13, name: 'Recipes3', description: "Recipe1 Recipe1 Recipe1 Recipe1 Recipe1 Recipe1 Recipe3 ",
        imageUrl: "http://2.bp.blogspot.com/-XEr0HFwm8f8/UKKpvvvUPuI/AAAAAAAATC4/3CciH7zAFBc/s200/DSC_0050.JPG",
         ingre: [ "Urad dal : 1 cup"," Orange color ","2 pinches Oil for deep fry" ] },
    { id: 14, name: 'Recipes4', description: "Recipe1 Recipe1 Recipe1 Recipe1 Recipe1 Recipe1 Recipe4 ",
        imageUrl: "http://1.bp.blogspot.com/--Bl1f4zpO4U/UJ15BrCWt6I/AAAAAAAAS9Y/dJhlujqkYuU/s200/DSC_0032.JPG",
        ingre: [ "Urad dal : 1 cup"," Orange color ","2 pinches Oil for deep fry" ] }
];


export default class App extends Component {
  
   constructor(props) {
      super(props);
       const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
       this.state = {
           dataSource: ds.cloneWithRows(RECIPES),
        };
   
    }
   getInitialState() {
  
  }
  _onPressButton() {
    
  }
  _renderDetails = (rowData) => {
    let detailsStr = [];
     for(var i=0; i <rowData.ingre.length;++i) {
       detailsStr.push(<Text>{rowData.ingre[i]}</Text>);
     }
    return detailsStr;
    
  } 
   _renderRow = (rowData) => {
     return ( 
       <TouchableHighlight underlayColor = {'#ccc'} onPress={this._onPressButton}>
          <View style={styles.item}>
                   <View style={{ justifyContent: 'center', alignItems: 'center'}}>
                         <Image  source={{url:rowData.imageUrl}} 
                         style={{width:this._getWidth(95) , height: 200}} />
                    </View>
                    <Text style={styles.name}>  
                        {rowData.name} 
                    </Text>
                    <Text>
                         {rowData.description}
                    </Text>
                    
                    <View>
                      { this._renderDetails(rowData)}
                    </View>
              </View>
          </TouchableHighlight>    
        );
  };
   _renderSeparator(sectionID: number, rowID: number, adjacentRowHighlighted: bool) {
    return (
      <View
        key={`${sectionID}-${rowID}`}
        style={{
          height: adjacentRowHighlighted ? 4 : 1,
          backgroundColor: adjacentRowHighlighted ? '#3B5998' : '#CCCCCC',
        }}
      />
    );
  }
  
  render() {
    return (
      <View style={styles.content}>
         <View style={styles.header}>
            <Text style={styles.title}> Header </Text>
          </View>
        <ListView dataSource={this.state.dataSource}
             renderRow={this._renderRow}>
        </ListView>
      </View>
    );
 }
 
 _getWidth(percent) {
    return  Dimensions.get('window').width * (percent/100);
 }
 _getHeight(percent) {
    return  Dimensions.get('window').height * (percent/100);
 }

}


const styles = StyleSheet.create({
     item : {
      backgroundColor: '#f7f7f7',
       margin:5,
       contentAlign:'center',
       bordeWidth: 2
    },
    header : {
      backgroundColor:'#b7b7b7',
      height:50
    },
    title : {
      textAlign:'center',
      fontSize:24
    },
    name : {
      fontSize: 20,
      textAlign:'left',
      padding: 5
    },
    content : {
      marginTop: 20,
      backgroundColor:'#ddd',
      marginBottom:20
    }
});

